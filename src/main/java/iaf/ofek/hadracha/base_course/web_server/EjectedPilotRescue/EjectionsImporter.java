package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.Data.CrudDataBase;
import iaf.ofek.hadracha.base_course.web_server.Data.Entity;
import iaf.ofek.hadracha.base_course.web_server.Utilities.ListOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class EjectionsImporter {

    @Value("${ejections.server.url}")
    public String EJECTION_SERVER_URL;
    @Value("${ejections.namespace}")
    public String NAMESPACE;



    private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
    private final RestTemplate restTemplate;
    private final CrudDataBase dataBase;
    private final ListOperations listOperations;
    private static final Double SHIFT_NORTH = 1.7;

    public EjectionsImporter(RestTemplateBuilder restTemplateBuilder, CrudDataBase dataBase, ListOperations listOperations) {
        restTemplate = restTemplateBuilder.build();
        this.dataBase = dataBase;
        this.listOperations = listOperations;
        executor.scheduleAtFixedRate(this::updateEjections, 1, 1, TimeUnit.SECONDS);

    }

    private void updateEjections() {
        try {
            List<EjectedPilotInfo> updatedEjections = this.getEjectionsFromServer();
            List<EjectedPilotInfo> previousEjections = dataBase.getAllOfType(EjectedPilotInfo.class);

            List<EjectedPilotInfo> addedEjections = this.ejectionsDifference(updatedEjections, previousEjections);
            List<EjectedPilotInfo> removedEjections = this.ejectionsDifference(previousEjections, updatedEjections);

            addedEjections.forEach(dataBase::create);
            removedEjections.stream().map(EjectedPilotInfo::getId).forEach(id -> dataBase.delete(id, EjectedPilotInfo.class));
        } catch (RestClientException e) {
            System.err.println("Could not get ejections: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private List<EjectedPilotInfo> ejectionsDifference(List<EjectedPilotInfo> subtractedEjections, List<EjectedPilotInfo> subtractingEjections) {
        return listOperations.subtract(subtractedEjections, subtractingEjections, new Entity.ByIdEqualizer<>());
    }

    private List<EjectedPilotInfo> getEjectionsFromServer() {
        List<EjectedPilotInfo> ejectionsFromServer;
        ResponseEntity<List<EjectedPilotInfo>> responseEntity = restTemplate.exchange(
                EJECTION_SERVER_URL + "/ejections?name=" + NAMESPACE, HttpMethod.GET,
                null, new ParameterizedTypeReference<List<EjectedPilotInfo>>() {
                });
        ejectionsFromServer = responseEntity.getBody();

        this.shiftNorthEjectionInfos(ejectionsFromServer);

        return ejectionsFromServer;
    }

    private void shiftNorthEjectionInfos(List<EjectedPilotInfo> ejectedPilotInfos) {
        if (ejectedPilotInfos != null) {
            for(EjectedPilotInfo ejectedPilotInfo: ejectedPilotInfos) {
                ejectedPilotInfo.getCoordinates().lat += SHIFT_NORTH;
            }
        }
    }
}
